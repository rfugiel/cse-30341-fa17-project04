#!/bin/bash

test-library() {
    library=$1
    echo "Testing $library ... "
    echo
   timer=$(time env LD_PRELOAD=./lib/$library ./bin/benchmark)
   echo "$timer"
    benchmark=$(env LD_PRELOAD=./lib/$library ./bin/benchmark)

	echo
	
    	reuses=$(echo "$benchmark" | grep "reuses" | cut -d ":" -f 2 | sed -e 's/^[ \t]*//')
    	heap=$(echo "$benchmark" | grep "max heap" | cut -d ":" -f 2 | sed -e 's/^[ \t]*//')
    	coalesce=$(echo "$benchmark" | grep "coalesces" | cut -d ":" -f 2 | sed -e 's/^[ \t]*//')
    	splits=$(echo "$benchmark" | grep "splits" | cut -d ":" -f 2 | sed -e 's/^[ \t]*//')
	echo "    REUSES: $reuses"
	echo "    HEAP SPACE: $heap"
	echo "    COALESCES:  $coalesce"
	echo "    SPLITS:     $splits"
	echo
}


test-library libmalloc-ff.so
test-library libmalloc-nf.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so
