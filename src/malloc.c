/* malloc.c: simple memory allocator -----------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

/* Macros --------------------------------------------------------------------*/

#define ALIGN4(s)           (((((s) - 1) >> 2) << 2) + 4)
#define BLOCK_DATA(b)       ((b) + 1)
#define BLOCK_HEADER(ptr)   ((struct block *)(ptr) - 1)

/* Block structure -----------------------------------------------------------*/

struct block {
    size_t        size;
    struct block *next;
    bool          free;
};

/* Global variables ----------------------------------------------------------*/

struct block *FreeList = NULL;      // keeps track of all block allocations
struct block *last_block = NULL;    // keeps track of last block we searched

// Counter Variables
int count_malloc = 0;       // successful malloc calls
int count_free = 0;         // successfull free calls
int count_reuse = 0;        // reuse existing block
int count_grow = 0;         // request new block
int count_split = 0;        // split a block
int count_coalesce = 0;     // coalesce block
int count_block = 0;        // blocks in free list
size_t count_requested = 0; // total memory requested
size_t count_maxheap = 0;   // max heap size

// Ensures "atexit()" only registers once
bool exit_registered = false;

/* Display info at exit ------*/

void onExit() {
  struct block * iter = FreeList;
  while(iter) {
    count_block++;
    iter = iter->next;
  }

  printf("mallocs:   %d\n", count_malloc);
  printf("frees:     %d\n", count_free);
  printf("reuses:    %d\n", count_reuse);
  printf("grows:     %d\n", count_grow);
  printf("splits:    %d\n", count_split);
  printf("coalesces: %d\n", count_coalesce);
  printf("blocks:    %d\n", count_block);
  printf("requested: %zu\n", count_requested);
  printf("max heap:  %zu\n", count_maxheap);
}

/* Find free block -----------------------------------------------------------*/

struct block *find_free(struct block **last, size_t size) {
    struct block *curr = FreeList;

// ----- POLICIES ----- //

/* First Fit */
    #if defined FIT && FIT == 0
        while (curr && !(curr->free && curr->size >= size)) {
            *last = curr;
            curr  = curr->next;
        }
    #endif

/* Next Fit */
    #if defined FIT && FIT == 1
        if (!last_block) {  // Get last block, set to start of freeList if NULL
            last_block = FreeList;
        }
        curr = last_block;

        //Do first fit search
        while (curr && !(curr->free && curr->size >= size)) {
            *last = curr;
            curr  = curr->next;
        }

        // "Loop around" past end if nothing found
        if (!curr) {
            curr = FreeList;
            while (curr && !(curr->free && curr->size >= size)) {
                *last = curr;
                curr  = curr->next;
            }
            //if still not found, set to NULL
            if (curr == last_block) {
                curr = NULL;
            }
        }
        //set last searched block
        last_block = curr;
    #endif

/* Best Fit */
    #if defined FIT && FIT == 2
        struct block * best_fit = NULL;
        // Go through freeList and find best fit
        while (curr) {
            if (curr->free && curr->size >= size) { //if can fit...
                if (!best_fit) {  //initialize if not already assigned 
                    best_fit = curr;
                } else if (curr->size < best_fit->size) {   //check if curr is a better fit
                    best_fit = curr;
                }
            }
            *last = curr;
            curr  = curr->next;
        }

        curr = best_fit;
    #endif

/* Worst Fit */
    #if defined FIT && FIT == 3
        //Opposite of best fit...just check if larger isntead of smaller
        struct block * worst_fit = NULL;
        while (curr) {
            if (curr->free && curr->size >= size) {
                if (!worst_fit) {
                    worst_fit = curr;
                } else if (curr->size > worst_fit->size) {
                    worst_fit = curr;
                }
            }
            *last = curr;
            curr  = curr->next;
        }
        curr = worst_fit;
    #endif

// Returns free block to use
    return curr;
}

/* Grow heap -----------------------------------------------------------------*/

struct block *grow_heap(struct block *last, size_t size) {
    /* Update trackers */
    count_grow++;
    count_maxheap += size;

    /* Request more space from OS */
    struct block *curr = (struct block *)sbrk(0);
    struct block *prev = (struct block *)sbrk(sizeof(struct block) + size);

    assert(curr == prev);

    /* OS allocation failed */
    if (curr == (struct block *)-1) {
        return NULL;
    }

    /* Update FreeList if not set */
    if (FreeList == NULL) {
        FreeList = curr;
    }

    /* Attach new block to prev block */
    if (last) {
        last->next = curr;
    }

    /* Update block metadata */
    curr->size = size;
    curr->next = NULL;
    curr->free = false;
    return curr;
}

/* Allocate space ------------------------------------------------------------*/

void *malloc(size_t size) {
    //register "atexit()" function
    if (!exit_registered) {
        int i = atexit(onExit);
           if (i != 0) {
               fprintf(stderr, "cannot set exit function\n");
               exit(EXIT_FAILURE);
           }
        exit_registered = true;
    }

    /* Align to multiple of 4 */
    size = ALIGN4(size);

    /* Update tracker */
    count_requested += size;

    /* Handle 0 size */
    if (size == 0) {
        return NULL;
    }

    /* Look for free block */
    struct block *last = last_block;
    struct block *next = find_free(&last, size);

    /* Split free block */

    //   |          NEXT          | <--- All free

    //   |  NEXT  |   NEW BLOCK   |  <-- NEXT is filled, NEW BLOCK is free

    size_t newBlock_size = (size + sizeof(struct block));           // size required to create new block

    if(next && (next->size > newBlock_size)) {                      // check if over-allocated (extra free space)
        struct block *new_block = (void*)BLOCK_DATA(next) + size;   // create new block
        new_block->size = next->size - newBlock_size;               // set size to equal "leftover" space 
        new_block->free = true;                                     // mark as free
        new_block->next = next->next;                                // set next

        /* Fill in "next" struct values */
        next->size = size;
        next->free = false;
        next->next = new_block; 
       
       /* Increment counter */
        count_split++;
    }


    /* Could not find free block, so grow heap */
    if (next == NULL) {
        next = grow_heap(last, size);
    }else{
        count_reuse++;
    }

    /* Could not find free block or grow heap, so just return NULL */
    if (next == NULL) {
        return NULL;
    }
    
    /* Mark block as in use */
    next->free = false;

    /* Increment counter */
    count_malloc++;

    /* Return data address associated with block */
    return BLOCK_DATA(next);

}

/* Reclaim space -------------------------------------------------------------*/

void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }

    /* Make block as free */
    struct block *curr = BLOCK_HEADER(ptr);
    assert(curr->free == 0);
    curr->free = true;

    /* Increment counter */
    count_free++;

    /* Coalesce free blocks */   
    struct block *iter = curr; //set starting point 

    while (iter && iter->next) {
        if (iter->free && iter->next->free) {
            iter->size += sizeof(struct block) + iter->next->size;
            iter->next = iter->next->next;
            count_coalesce++;
        }
        iter = iter->next;
    }

}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: --------------------------------*/
