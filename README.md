CSE.30341.FA17: Project 04
==========================

This is the documentation for [Project 04] of [CSE.30341.FA17].

Members
-------

1. Radomir Fugiel (rfugiel@nd.edu)

Design
------

> 1. You will need to implement splitting of free blocks:
>
>   - When should you split a block?

You should split when you use less space than the block has (split the leftover)

>   - How should you split a block?

You should split by downsizing the block you allocate memory to and then make a new block of free memory that
is the size you shrunk the block by.

Response.

> 2. You will need to implement coalescing of free blocks:
>
>   - When should you coalescing block?

You should coalesce blocks when you free, combining continuous blocks of free memory.

>   - How should you coalesce a block?

You should check the adjacent blocks and if there are adjacent free blocks merge them into one block
Response.

> 3. You will need to implement Next Fit.
>
>   - What information do you need to perform a Next Fit?

We need to know the last block searched 

>   - How would you implement Next Fit?

From the last searched block, I will use first fit

Response.

> 4. You will need to implement Best Fit.
>
>   - What information do you need to perform a Best Fit?

We need to know the amount of memory we are allocating

>   - How would you implement Best Fit?

We loop through and find the the smallest block in size that the memory can fit in

Response.

> 5. You will need to implement Worst Fit.
>
>   - What information do you need to perform a Worst Fit?

We need to know the amount of memory we are allocating

>   - How would you implement Worst Fit?

We loop through and find the the largest block in size that the memory can fit in

Response.

> 6. You will need to implement tracking of different information.
>
>   - What information will you want to track?
I wil want to track info such as # of malloc calls, free calls, colaesces, splits, blocks in freelist, total memory requested and max heap size
>
>   - How will you update these trackers?

Within the library when I perform certain fucntions I will increment the associated tracker/counter


>   - How will you report these trackers when the program ends?

I will print them out when the program ends using the atexit() function


Response.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

https://docs.google.com/a/nd.edu/presentation/d/11bF5eSSCrLiv4TSOcXqBmhc9zqwbdSl2oScSPYh86SI/edit?usp=sharing

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 04]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project04.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
